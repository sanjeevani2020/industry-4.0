import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Create Next App</title>
		<link rel="icon" href="/favicon.ico" />
		
		  <meta charset="utf-8"/>
		  <meta name="viewport" content="width=device-width, initial-scale=1"/>
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"/>
		   <link href="/css/style.css" rel="stylesheet" />
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
      </Head>

	<nav class="navbar navbar-expand-md bg-dark navbar-dark ">
    
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse  justify-content-center" id="collapsibleNavbar">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link mx-2" href="industry4.html">Industry 4.0</a>
        </li>
        <li class="nav-item">
          <a class="nav-link mx-2" href="education4.html">Education 4.0</a>
        </li>
        <li class="nav-item">
          <a class="nav-link  mx-2" href="futureJobs.html">Future Jobs</a>
        </li>    
      </ul>
    </div> 
    
  </nav>
  <hr class="contentBreak" class="mx-4 " id="navHr"/> 
  <br/>


  <div class="container mx-auto text-center " id="industry4Img" >
    <img src="/img/industry4.0.jpg"  class="img-fluid" alt="Industry 4.0" style="widows: 400px; height:300px;" />
  </div>

  
  <div class="container mx-auto text-center" id="industry4header">
    
        <h3 id="top1header"> Industry 4.0</h3>
        <hr class="contentBreak" class="mx-auto "/>
        <h4 class="industry4sub">What is Industry 4.0?</h4>
            <p class="subDesc">
                Industry 4.0 refers to the fourth industrial revolution and is related to industry, 
                although it is concerned with areas that are not usually classified as industry applications 
                in their own right, such as smart cities.

            </p>
            <p>
                Cyber-physical systems form the basis of Industry 4.0 (e.g., ‘smart machines’).
                 They use modern control systems, have embedded software systems and dispose of an Internet 
                 address to connect and be addressed via IoT (the Internet of Things). This way, products and means 
                 of production get networked and can ‘communicate’, enabling new ways of production,
                 value creation, and real-time optimization. Cyber-physical systems create the capabilities needed for smart factories.
            </p>
        <h4 class="industry4sub">Why Industry 4.0?</h4>
        <p class="subDesc">
            The I4.0 discussion within an enterprise should begin with the business strategy. 
            Where is the company headed? Is top-line growth the priority, and is increased capacity to meet
             demand the focus for operations? Is the business focused on reducing costs to remain competitive in 
             a market with tightening margins? And what about manufacturing flexibility? How is the company addressing
              new market pressures, such as the ability to meet customization demands?

            Understanding these strategic objectives is vital to ensure that subsequent discussions of how to achieve
             these goals are smart discussions.
            
            To achieve these business goals in this digital era, operations leadership 
            (and specifically the manufacturing operations of a company) must identify digitalization projects that align with the business objectives. Examples include reducing risk and addressing compliance requirements, which align with operational projects that address track-and-trace solutions. To do this,
             secure connectivity of automation systems and the strategic movement of data are critical.

        </p>
        
        <h4 class="industry4sub">Industry 4.0 in...</h4>
        
        
        
        <section id="imageContainer" class="mx-5">
            <div class="row mx-5">
                <div class="col">
                    <a href="iot.html#indutry4">
                        <img class="img-fluid subImg" src="/img/industryiot.jpg" alt="Internet Of Things"/>
                    </a>
                    

                </div>
                <div class="col">
                    <a href="robotics.html#industry4">
                        <img class="img-fluid subImg"  src="/img/industry4Robo.jpg" alt="Robotics"/>
                    </a>
                    
                    
                </div>
                <div class="col">
                    <a href="blockchain.html#industry4">
                        <img class="img-fluid subImg"  src="/img/industry4blockchain.jpg" alt="blockchain"/> 
                    </a>
                    
                    
                </div>
                <div class="col">
                    <a href="cloud.html#industry4">
                        <img class="img-fluid subImg"  src="/img/industry4Cloud.jpg" alt="cloudComputing"/>
                    </a>
                    
                    
                </div>
            </div>
            <br/>
            <div class="row mx-5">
                <div class="col">
                    <a href="3dprint.html#industry4">
                        <img class="img-fluid subImg" src="/img/insutry43d.jpg" alt="3dprinting"/>
                    </a>
                    
                    
                </div>
                <div class="col">
                    <a href="security.html#industry4">
                        <img class="img-fluid subImg" src="/img/industry4security.jpg" alt="CyberSecurity"/>
                    </a>
                    
                    
                </div>
                <div class="col">
                    <a href="bigdata.html#industry4">
                        <img class="img-fluid subImg" src="/img/industry4bigData.jpg" alt="BigData"/>
                    </a>
                    
                    
                </div>
                <div class="col">
                    <a href="ai.html#industry4">
                        <img class="img-fluid subImg" src="/img/industry4ai.jpg" alt="ArtificialInelligence"/>
                    </a>
                    
                    
                </div>
            </div>
        </section>
        <br/>
        
        <div id="dvFooter">
            
            <div class="row bg-dark">
                <hr class="contentBreak" class="mx-4 " id="footerHr"/>
                <div class="col">
                    <ul>
                        <li>Industry 4.0</li>
                        <li>Education 4.0</li>
                        <li>Future Jobs</li>
                    </ul>
                </div>
                <div class="col">
                    <ul>
                        <li>Internet of Things</li>
                        <li>Robotics</li>
                        <li>BlockChain</li>
                        <li>Cloud Computing</li>
                        <li>3D Printing</li>
                        <li>Cyber Security</li>
                        <li>Big Data</li>
                        <li>Artificial Intelligence</li>
                    </ul>
                </div>

                <div class="col">
                    <h6>Conatct Us</h6>
                    <p>
                        email: abc.pqr@gmail.com
                        <br/>
                        phone: 9876054321
                    </p>
                </div>
            </div>
        </div>

      
  </div>
  



     
    </div>
  )
}
